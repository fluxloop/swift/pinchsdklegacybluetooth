// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "PinchSDKLegacyBluetooth",
    products: [
        .library(
            name: "PinchSDKLegacyBluetooth",
            targets: ["PinchSDKLegacyBluetooth"]
        )
    ],
    dependencies: [

    ],
    targets: [
        .binaryTarget(
            name: "PinchSDKLegacyBluetooth",
            url: "https://puresdk.blob.core.windows.net/pinchsdk-versions/xcf/PinchSDKLegacyBluetooth.1.0.103.zip",
            checksum: "0110ce10e49f65808db0bab967a1b2a804df10554adbea2d55d64f1db4013179"
            
        )
    ]
)
